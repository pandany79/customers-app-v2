import React from 'react';
import { Router, Route, Switch } from 'react-router-dom'
import history from './helpers/history'
import Header from './components/Header'
import CustomerList from './components/customers/CustomerList'
import CustomerCreate from './components/customers/CustomerCreate'
import CustomerDetails from './components/customers/CustomerDetails'
import CustomerEdit from './components/customers/CustomerEdit'
import CustomerDelete from './components/customers/CustomerDelete'

const App = () => {
  return (
    <Router history={ history }>
      <div className="ui container">
        <Header />
        <Switch>
          <Route path="/" exact component={ CustomerList } />
          <Route path="/customers/new" component={ CustomerCreate } />
          <Route path="/customers/edit/:id" component={ CustomerEdit } />
          <Route path="/customers/delete/:id" component={ CustomerDelete } />
          <Route path="/customers/:id" component={ CustomerDetails } />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
