import api from './../api'
import {
  SIGN_IN,
  SIGN_OUT,
  CREATE_CUSTOMER,
  GET_CUSTOMERS,
  GET_CUSTOMER,
  EDIT_CUSTOMER,
  DELETE_CUSTOMER
} from './types'
import history from './../helpers/history'

/**
 * Dispatches the Sign In action.
 * @param {String} userId 
 */
export const signIn = userId => {
  return {
    type: SIGN_IN,
    payload: userId
  }
}

/**
 * Dispatches the Sign Out action.
 */
export const signOut = () => {
  return {
    type: SIGN_OUT
  }
}

/**
 * Dispatches the Create Customer action.
 * @param {Object} formValues 
 */
export const postCustomer = formValues => async (dispatch, getState) => {
  // Gets the "userId" from the "getState" function (access to redux State).
  const { userId } = getState().auth

  // Creates the customer into the JSON object.
  const customer = (await api.post('/customers', { ...formValues, registeredBy: userId })).data

  // Dispatches the Create Customer action once we get the axios result.
  dispatch({
    type: CREATE_CUSTOMER,
    payload: customer
  })

  // Go back to the customers list.
  history.push('/')
}

/**
 * Dispatches the Get Customers action.
 * @param {Object} formValues 
 */
export const getCustomers = () => async dispatch => {
  // Fetches the customer list from the JSON object.
  const customers = (await api.get('/customers')).data

  // Dispatches the Get Customers action once we get the axios result.
  dispatch({
    type: GET_CUSTOMERS,
    payload: customers
  })
}

/**
 * Dispatches the Get Customer action.
 * @param {Number} id
 */
export const getCustomer = id => async dispatch => {
  // Fetches a specific customer from the JSON object.
  const customer = (await api.get(`/customers/${id}`)).data

  // Dispatches the Get Customer action once we get the axios result.
  dispatch({
    type: GET_CUSTOMER,
    payload: customer
  })
}

/**
 * Dispatches the Edit Customer action.
 * @param {Number} id
 * @param {Object} formValues 
 */
export const patchCustomer = (id, formValues) => async dispatch => {
  // Updates a customer into the JSON object.
  const customer = (await api.patch(`/customers/${id}`, formValues)).data

  // Dispatches the Edit Customer action once we get the axios result.
  dispatch({
    type: EDIT_CUSTOMER,
    payload: customer
  })

  // Go back to the customers list.
  history.push('/')
}

/**
 * Dispatches the Delete Customer action.
 * @param {Number} id 
 */
export const deleteCustomer = id => async dispatch => {
  // Deletes a customer from the JSON object.
  await api.delete(`/customers/${id}`)

  // Dispatches the Delete Customer action once we get the axios result.
  dispatch({
    type: DELETE_CUSTOMER,
    payload: id
  })

  // Go back to the customers list.
  history.push('/')
}
