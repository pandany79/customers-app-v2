import React from 'react'
import { Link } from 'react-router-dom'
import GoogleAuth from './../helpers/GoogleAuth'

const Header = () => {
  return (
    <div className="ui secondary pointing menu">
      <Link
        to="/"
        className="item"
      >
        Customers App v2
      </Link>
      <div className="right menu">
        <Link
          to="/"
          className="item"
        >
          View Customers List
        </Link>
        <GoogleAuth />
      </div>
    </div>
  )
}

export default Header