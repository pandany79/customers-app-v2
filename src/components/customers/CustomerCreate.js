import React from 'react';
import { connect } from 'react-redux'
import { postCustomer } from './../../actions'
import CustomerForm from './CustomerForm'

const CustomerCreate = (props) => {
  const { isSignedIn } = props

  const onSubmit = formValues => {
    props.postCustomer(formValues)
  }

  if (isSignedIn === null) {
    return <div>Loading...</div>
  } else if (!isSignedIn) {
    return (
      <div className="content">
        <h1>Please Login</h1>
      </div>
    )
  }

  return (
    <div>
      <h3>Create New Customer</h3>
      <CustomerForm onSubmit={ onSubmit } />
    </div>
  )
}

const mapStateToProps = state => {
  return {
    isSignedIn: state.auth.isSignedIn
  }
}

export default connect(
  mapStateToProps,
  { postCustomer }
)(CustomerCreate)