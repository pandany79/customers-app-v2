import React, { useEffect, Fragment } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { getCustomer, deleteCustomer } from './../../actions'
import Modal from './../Modal'
import history from './../../helpers/history'

const CustomerDelete = (props) => {
  const { id } = props.match.params
  const { isSignedIn, customer } = props

  useEffect(() => {
    props.getCustomer(id)
  }, [])

  const onDeleteClick = () => {
    props.deleteCustomer(id)
  }

  const actions = (
    <Fragment>
      <button
        className="ui button red"
        onClick={ onDeleteClick }
      >
        <i className="trash alternate icon" />
        Delete
      </button>
      <Link
        to="/"
        className="ui button"
      >
        Cancel
      </Link>
    </Fragment>
  )

  if (isSignedIn === null) {
    return <div>Loading...</div>
  } else if (!isSignedIn) {
    return (
      <div className="content">
        <h1>Please Login</h1>
      </div>
    )
  } else if (!customer) {
    return null
  }

  return (
    <Modal
      header="Delete Customer"
      content={ `Are you sure you want to delete the customer ${customer.name}?` }
      actions={ actions }
      onDismiss={ () => history.push('/') }
    />
  )
}

const mapStateToProps = (state, ownProps) => {
  const { id } = ownProps.match.params
  return {
    isSignedIn: state.auth.isSignedIn,
    customer: state.customers[id]
  }
}

export default connect(
  mapStateToProps,
  { getCustomer, deleteCustomer }
)(CustomerDelete)