import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { getCustomer } from './../../actions'

const CustomerDetails = (props) => {
  const { id } = props.match.params
  const { isSignedIn, customer } = props

  useEffect(() => {
    props.getCustomer(id)
  }, [])

  if (isSignedIn === null) {
    return <div>Loading...</div>
  } else if (!isSignedIn) {
    return (
      <div className="content">
        <h1>Please Login</h1>
      </div>
    )
  }

  const { name, age, code } = customer
  return (
    <div>
      <h1>{ name }</h1>
      <h5>{ `Age: ${age} years` }</h5>
      <h5>{ `Code: ${code}` }</h5>
      <Link
        to="/"
        className="ui button primary"
      >
        <i className="chevron left icon" />
        Go back to List
      </Link>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const { id } = ownProps.match.params
  console.log()
  return {
    isSignedIn: state.auth.isSignedIn,
    customer: state.customers[id]
  }
}

export default connect(
  mapStateToProps,
  { getCustomer }
)(CustomerDetails);