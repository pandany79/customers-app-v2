import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import { getCustomer, patchCustomer } from './../../actions'
import CustomerForm from './CustomerForm'

const CustomerEdit = (props) => {
  const { id } = props.match.params
  const { isSignedIn, customer } = props

  useEffect(() => {
    props.getCustomer(id)
  }, [])

  const onSubmit = formValues => {
    props.patchCustomer(id, formValues)
  }

  if (isSignedIn === null) {
    return <div>Loading...</div>
  } else if (!isSignedIn) {
    return (
      <div className="content">
        <h1>Please Login</h1>
      </div>
    )
  }

  console.log(customer)
  return (
    <div>
      <h3>Edit Customer</h3>
      <CustomerForm
        initialValues={ _.pick(customer, 'name', 'age', 'code') }
        onSubmit={ onSubmit }
      />
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  const { id } = ownProps.match.params
  return {
    isSignedIn: state.auth.isSignedIn,
    customer: state.customers[id]
  }
}

export default connect(
  mapStateToProps,
  { getCustomer, patchCustomer }
)(CustomerEdit)