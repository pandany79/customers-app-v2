import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { getCustomers } from '../../actions'
import { Link } from 'react-router-dom'

const CustomerList = (props) => {
  const { isSignedIn, customers } = props
  useEffect(() => {
    props.getCustomers()
  }, [])

  const renderList = () => {
    if (!customers.length) {
      return (
        <div className="item">
          There are no registered Customers
        </div>
      )
    }

    return customers.map(customer => {
      return (
        <div
          key={ customer.id }
          className="item"
        >
          <div className="right floated content">
            <Link
              to={ `/customers/edit/${customer.id}` }
              className="ui button primary"
            >
              <i className="edit icon" />
              Edit
            </Link>
            <Link
              to={ `/customers/delete/${customer.id}` }
              className="ui button negative"
            >
              <i className="trash alternate icon" />
              Delete
            </Link>
          </div>
          <div className="content">
            <Link
              to={`/customers/${customer.id}`}
              className="header"
            >
              { customer.name }
            </Link>
          </div>
        </div>
      )
    })
  }

  const renderCreate = () => {
    return (
      <div style={{ textAlign: 'right' }}>
        <Link
          to="/customers/new"
          className="ui button primary"
        >
          <i className="user plus icon" />
          Create Customer
        </Link>
      </div>
    )
  }

  if (isSignedIn === null) {
    return <div>Loading...</div>
  } else if (!isSignedIn) {
    return (
      <div className="content">
        <h1>Please Login</h1>
      </div>
    )
  }

  return (
    <div>
      <h2>Customers List</h2>
      <div className="ui celled list">
        { renderList() }
      </div>
      { renderCreate() }
    </div>
  )
}

const mapStateToProps = state => {
  return {
    isSignedIn: state.auth.isSignedIn,
    customers: Object.values(state.customers)
  }
}

export default connect(
  mapStateToProps,
  { getCustomers }
)(CustomerList)