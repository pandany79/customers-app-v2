import _ from 'lodash'
import {
  CREATE_CUSTOMER,
  GET_CUSTOMERS,
  GET_CUSTOMER,
  EDIT_CUSTOMER,
  DELETE_CUSTOMER
} from './../actions/types'

export default (state = {}, action) => {
  switch (action.type) {
    case GET_CUSTOMERS:
      return { ...state, ..._.mapKeys(action.payload, 'id') }
    case CREATE_CUSTOMER:
    case GET_CUSTOMER:
    case EDIT_CUSTOMER:
      return { ...state, [action.payload.id]: action.payload }
    case DELETE_CUSTOMER:
      return _.omit(state, action.payload)
    default:
      return state
  }
}